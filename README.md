
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to …

## Installation

You can install the development version of squirrels like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
library(glue)
message("We will focus on Cinnamon squirrels")
#> We will focus on Cinnamon squirrels
primary_fur_color <- "Cinnamon"
message(glue("We will focus on {primary_fur_color} squirrels"))
#> We will focus on Cinnamon squirrels
## basic example code
```
